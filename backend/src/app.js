require('dotenv').config();

const express         = require('express');
const app             = express();
const path            = require('path');
const connectDB       = require("./db");
const link            = require("./constants");
const extensionRoutes = require('./routes/extension');
const logoRoutes      = require('./routes/logo');
const userRoutes      = require('./routes/user');
// start const to test REST API
const Extension       = require('./models/extension');
const Logo       = require('./models/logo');
const User            = require('./models/user');
// end const to test REST API
app.disable("x-powered-by");
connectDB();
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
  next();
});

app.use(express.json());
app.use('/images', express.static(path.join(__dirname, 'images')));

app.use(link['USER_URI'],userRoutes);
//objects: add routes objects
app.use(link['PRODUCT_URI'],extensionRoutes,logoRoutes);

//Start test Rest API
//start object 1: extension
app.post(link['API_PRODUCT_URI'],(req,res,next) => {
    delete req.body._id;
    const extension = new Extension({...req.body});
    extension.save()
    .then(() => res.status(200).json({message: 'new extension created'}))
    .catch(error => res.status(400).json({ error }));
});

app.use(link['API_PRODUCT_URI'],(req, res, next) => {
    Extension.find()
    .then(extension => res.status(200).json(extension))
    .catch(error => res.status(400).json({ error }));
});
//end object 1: extension
//start object2: logo
app.post(link['API_LOGO_URI'],(req,res,next) => {
    delete req.body._id;
    const logo = new Logo({...req.body});
    logo.save()
    .then(() => res.status(200).json({message: 'new logo created'}))
    .catch(error => res.status(400).json({ error }));
});

app.use(link['API_LOGO_URI'],(req, res, next) => {
    Logo.find()
    .then(logo => res.status(200).json(logo))
    .catch(error => res.status(400).json({ error }));
});
//end object 2: logo
app.post(link['API_USER_URI'],(req,res,next) => {
    delete req.body._id;
    const user = new User({...req.body});
    user.save()
    .then(() => res.status(200).json({message: 'new user created'}))
    .catch(error => res.status(400).json({ error }));
});

app.use(link['API_USER_URI'],(req, res, next) => {
    User.find()
    .then(user => res.status(200).json(user))
    .catch(error => res.status(400).json({ error }));
});
// End test Rest API
module.exports = app;