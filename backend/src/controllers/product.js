const fs        = require('fs');

// CREATE
module.exports.createObj = (var0,var1) => {
  const createObj   = (req, res, next) => {
      const obj = JSON.parse(req.body.product);
      delete obj._id;
      const product = new var1({
          ...obj,
          imageUrl: `${req.protocol}://${req.get('host')}${var0}${req.file.filename}`
      });
      product.save()
      .then(() => res.status(201).json({ message: 'Object registered successfully !'}))
      .catch(error => res.status(400).json({ error }));
  };
  return createObj;
}

// READ
module.exports.readOneProduct = (var0) => {
  const readOneProduct = (req, res, next) => {
    var0.findOne({ _id: req.params.id})
      .then(product => res.status(200).json(product))
      .catch(error => res.status(400).json({error}));
  };
  return readOneProduct;
}
// READ ALL
module.exports.readAllProducts = (var0) => {
  const readAllProducts = (req, res, next) => {
    var0.find()
  .then(product => res.status(200).json(product))
  .catch(error => res.status(400).json({error}));
  };
  return readAllProducts;
}
// UPDATE
module.exports.updateProduct = (var0,var1) => {
const updateProduct = (req, res, next) => {
  const prductObject = req.file ?
    {
      ...JSON.parse(req.body.product),
      imageUrl: `${req.protocol}://${req.get('host')}${var0}${req.file.filename}`
    } : { ...req.body };
  //(id envoyez dans les param de la req)     (nouvelle version de l'objet)(on s'arrure que c'est l'id contenu dans la route)
  var1.updateOne({ _id: req.params.id }, { ...prductObject, _id: req.params.id })
    .then(() => res.status(200).json({ message: 'Object modified successfully !'}))
    .catch(error => res.status(400).json({ error }));
  };
  return updateProduct;
}
// DELETE
module.exports.deleteOneProduct = (var0,var1) => {
  const deleteOneProduct = (req, res, next) => {
    var1.findOne({ _id: req.params.id })
      .then(product => {
        const filename = product.imageUrl.split(var0)[1];
        fs.unlink(`src${var0}${filename}`, () => {
          var1.deleteOne({ _id: req.params.id })
            .then(() => res.status(200).json({ message: 'Objet supprimé !'}))
            .catch(error => res.status(400).json({ error }));
        });
      })
      .catch(error => res.status(500).json({ error }));
  };
  return deleteOneProduct;
}

// Create collections
module.exports.createCollection = (var0) => {
  const Collection = require('../models/'+var0);
  return Collection;
} 
