const bcrypt   = require('bcrypt');
const jwt      = require('jsonwebtoken');
const User     = require('../models/user');
const fs       = require('fs');
const RSA_PRIVATE_KEY = fs.readFileSync('src/security/id_rsa','utf8');

exports.signUp = (req, res, next) => {
    bcrypt.hash(req.body.password,10)
    .then(hash => {
        const user = new User({
            name     : req.body.name,
            firstName: req.body.firstName,
            email    : req.body.email,
            role     : req.body.role,
            password : hash,
        });

        user.save()
            .then(() => res.status(201).json({ message: 'User created successfully !' }))
            .catch(error => res.status(400).json({ error }));
    })
    .catch(error => res.status(500).json({ error }));
};

exports.signIn = (req, res, next) => {
  User.findOne({ email: req.body.email })
    .then(user => {
      if (!user) {
        return res.status(401).json({ error: 'Utilisateur non trouvé !' });
      }
      bcrypt.compare(req.body.password, user.password)
        .then(valid => {
          if (!valid) {
            return res.status(401).json({ error: 'Mot de passe incorrect !' });
          }
          res.status(200).json({
          userId: user._id,
          role: user.role,
          token: jwt.sign({userId: user._id,role: user.role},RSA_PRIVATE_KEY,{expiresIn: '24h'})})
        })
        .catch(error => res.status(500).json({ error }));
    })
    .catch(error => res.status(500).json({ error }));
};
module.exports.readAllUsers = (var0) => {
  const readAllUsers = (req, res, nnext) => {
    var0.find()
    .then(user => res.status(200).json({user}))
    .catch(error => res.status(400).json({error}))
  }
  return readAllUsers;
}


