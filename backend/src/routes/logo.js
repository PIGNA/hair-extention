const express             = require('express');
const router              = express.Router();
const objCtrl             = require('../controllers/product');
const auth                = require('../middleware/auth');
const multer              = require('../middleware/multer-logo');
const constants           = require("../constants");


// CREATE
router.post(constants.LOGO_ROUTE,auth,multer,objCtrl.createObj(constants['LOGO_URI'],objCtrl.createCollection(constants['LOGO'])));
// READ
router.get(constants.LOGO_ID,objCtrl.readOneProduct(objCtrl.createCollection(constants['LOGO'])));
router.get(constants.LOGO_ROUTE,objCtrl.readAllProducts(objCtrl.createCollection(constants['LOGO'])));
// UPDATE
router.put(constants.LOGO_ID,auth,multer,objCtrl.updateProduct(constants['LOGO_URI'],objCtrl.createCollection(constants['LOGO'])));
// DELETE
router.delete(constants.LOGO_ID,auth,objCtrl.deleteOneProduct(constants['LOGO_URI'],objCtrl.createCollection(constants['LOGO'])));

module.exports            = router;