const express             = require('express');
const router              = express.Router();
const objCtrl             = require('../controllers/product');
const auth                = require('../middleware/auth');
const multer              = require('../middleware/multer-extension');
const constants           = require("../constants");


// CREATE
router.post(constants.EXTENSION_ROUTE,auth,multer,objCtrl.createObj(constants['BRESILIAN_URI'],objCtrl.createCollection(constants['EXTENSION'])));
// READ
router.get(constants.EXTENSION_ID,objCtrl.readOneProduct(objCtrl.createCollection(constants['EXTENSION'])));
router.get(constants.EXTENSION_ROUTE,objCtrl.readAllProducts(objCtrl.createCollection(constants['EXTENSION'])));
// UPDATE
router.put(constants.EXTENSION_ID,auth,multer,objCtrl.updateProduct(constants['BRESILIAN_URI'],objCtrl.createCollection(constants['EXTENSION'])));
// DELETE
router.delete(constants.EXTENSION_ID,auth,objCtrl.deleteOneProduct(constants['BRESILIAN_URI'],objCtrl.createCollection(constants['EXTENSION'])));

module.exports            = router;