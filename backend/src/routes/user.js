const express        = require('express');
const router         = express.Router();
const userController = require('../controllers/user');
const objCtrl        = require('../controllers/product');
const constants      = require("../constants");

router.post('/signup',userController.signUp);
router.post('/signin',userController.signIn);
router.get('/',userController
    .readAllUsers(objCtrl.createCollection(constants['USER'])));
module.exports = router;

