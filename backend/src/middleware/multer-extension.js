const multer = require('multer');
const constants   = require("../constants");
const multerUtils = require('./multer-config');

module.exports = multer({storage: multerUtils.fsDestination(constants['BRESILIAN_URI'])}).single('image');
