const jwt                = require('jsonwebtoken');
const fs                 = require('fs');
const RSA_PRIVATE_KEY    = fs.readFileSync('src/security/id_rsa','utf8');
module.exports           = (req, res, next) =>{
    try{
    const token          = req.headers.authorization.split(' ')[1];
    const decoratedToken = jwt.verify(token,RSA_PRIVATE_KEY);
    const userId         = decoratedToken.userId;
 
        if(req.body.userId && req.body.userId !== userId){
           throw new Error ('invalid user ID');
        } else{
            next();
        }
    }catch{
        res.status(401).json({error: new Error('Invalid request')});
    }
};