require('dotenv').config();
const mongoose = require('mongoose');

const connectDB = () => {
     mongoose.connect(
        process.env.DB_CONNECTION,
        {useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true }
    )
    .then(()  => console.log('Connection to DB succeeded'))
    .catch(() => console.log('Connection to DB failed'));
}
module.exports = connectDB;