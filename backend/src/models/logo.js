const mongoose   = require('mongoose');
const logoSchema = mongoose.Schema({
    title: { type: String, required: true},
    description: { type: String, required: true},
    imageUrl: { type: String, required: true},
    userId: { type: String, required: true},
});
// 'Extension' => 'extension' + 's' => extensions
module.exports = mongoose.model('logo', logoSchema)