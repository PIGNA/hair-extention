const mongoose        = require('mongoose');
const logAndVideoSchema = mongoose.Schema({
    title: {type: String, require:true},
    description: {type: String, required: true},
    videoUrl: {type: String, require: true}
});

module.exports = mongoose.model('logovideo', logAndVideoSchema)