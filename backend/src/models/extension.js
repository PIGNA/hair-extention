const mongoose   = require('mongoose');
const extensionSchema = mongoose.Schema({
    title: { type: String, required: true},
    description: { type: String, required: true},
    imageUrl: { type: String, required: true},
    price: { type: Number, required: true},
    userId: { type: String, required: true},
});
// 'Extension' => 'extension' + 's' => extensions
module.exports = mongoose.model('Extension', extensionSchema)