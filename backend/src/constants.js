const link = {
    'USER_URI'       :'/beautiful/user',
    'API_USER_URI'   :'/api/beautiful/user',
    'PRODUCT_URI'    :'/beautiful/product',
    'API_PRODUCT_URI':'/api/beautiful/extension',
    'API_LOGO_URI'   :'/api/beautiful/logo',
    'BRESILIAN_URI'  :'/images/bresilienne/' ,
    'BCKG_VIDEO_URI' :'/images/background/',
    'LOGO_URI'       :'/images/logo/',
    //collections
    'LOGO'           :'logo',
    'EXTENSION'      :'extension',
    'USER'           :'user',
    //routes
    'EXTENSION_ROUTE':'/extension',
    'EXTENSION_ID'   :'/extension/:id',
    'LOGO_ROUTE':'/logo',
    'LOGO_ID'   :'/logo/:id',

};
module.exports = link;