export class Extension {
  _id: string;
  title: string;
  description: string;
  imageUrl: string;
  price: number;
  userId: string;
}
