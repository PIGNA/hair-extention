export class LogoBackGrdVideo {
    _id: string;
    title: string;
    description: string;
    imageUrl: string;
    userId: string;
}