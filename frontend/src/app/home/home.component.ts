import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { Subscription } from 'rxjs';
import { User } from '../models/User.models';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  newProductUrl           = environment.ADD_PRODUCT;
  userId                  : string;
  userTab                 : User[] = [];
  userTabSub              : Subscription;
  arrAdminUserObj         : { [tag: string]: any; } = {};
  connectedUser: boolean;
  private USERS_URL       = environment.ROOT_URL +
  environment.SERVER_PORT +
  environment.SERVER_URL_USER;
  constructor(private router: Router,
              private auth  : AuthService) {
                this.userId = this.auth.userId;}

  ngOnInit(): void {
    this.initAllUsers();
  }
  initAllUsers(){
    this.userTabSub = this.auth.usersTab$.subscribe(
      (users) => {
        this.userTab = users;
        this.connectedUser = this.initAdminUserArray(users,this.arrAdminUserObj, this.userId);
      });
    this.auth.getAllUser(this.USERS_URL);
  }
  initAdminUserArray(tab:any,arrayObj:any, id: string): any {
    for(let obj of tab['user']){
      if(obj.role==environment.ADMIN_USER){
        arrayObj[obj._id]=obj.email;
      }
    }
    return arrayObj.hasOwnProperty(id);
  }
  onNavigate(product: string){
    this.router.navigate([product]);
  }
}
