import { Component, OnInit, ViewEncapsulation, OnDestroy, HostListener } from '@angular/core';
import { SideBarNavigationService } from '../services/side-bar-navigation.service';
import { RegisterModalFormService } from '../services/register-modal-form.service';
import { SizeDetectorService } from '../services/size-detector.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Role } from '../auth/sign/role';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { environment } from '../../environments/environment';
import { UtilService } from '../services/util.service';
import { mimeType } from '../products/mime-type.validator';
import { CrudService } from '../services/crud.service';
import { LogoBackGrdVideo } from '../models/LogoBackGrdVideo.models';
import { User } from '../models/User.models';
import { LogoVideoService } from '../services/logo-video.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HeaderComponent implements OnInit, OnDestroy {
  productsUrl              = environment.PRODUCTS_URL;
  signupForm              :FormGroup;
  signInForm              :FormGroup;
  logoForm                : FormGroup;
  isButtonVisible         : boolean;
  isButtonVisibleSub      : Subscription;
  loading                 = false;
  errorMessage            : string;
  token                   : string;
  tokenSub                : Subscription;
  screenWidth             : number;
  screenWidthSub          : Subscription
  screenWidthCode         : string;
  userId                  : string;
  changeBackgroundColor   : boolean;
  changeBackgroundColorSub: Subscription;
  logoVideoTab            : LogoBackGrdVideo[] = [];
  logoVideoSub            : Subscription;
  userTab                 : User[] = [];
  userTabSub              : Subscription;
  private URL             = environment.ROOT_URL +
   environment.SERVER_PORT +
   environment.SERVER_URL_LOGO_BCKVIDEO;
  private USERS_URL       = environment.ROOT_URL +
   environment.SERVER_PORT +
   environment.SERVER_URL_USER;
  logo                    : string;
  checkedLogoId           : string;
  checkedImageUrl         : string;
  arrObj                  : { [tag: string]: any; } = {};
  arrAdminUserObj         : { [tag: string]: any; } = {};
  connectedUser           : boolean;
  isLogoChecked           : boolean;

  constructor(private formBuilder            : FormBuilder,
              private navService             : SideBarNavigationService,
              private navLoginRegisterService: RegisterModalFormService,
              private authService            : AuthService,
              private sizeDetectorService    : SizeDetectorService,
              private router                 : Router,
              private util                   : UtilService,
              private crudService            : CrudService,
              private logoVideoService       : LogoVideoService
              )
              {
              this.userId                     = this.authService.userId;
          }

  ngOnInit() {
    this.initSignupForm();
    this.initSigninForm();
    this.initAuthSub();
    this.initBackGroundColor();
    this.initLogoFom();
    this.emitButton();
    this.onResize();
    this.initLogoVideoList();
    this.initAllUsers();
  }
  initAllUsers(){
    this.userTabSub = this.authService.usersTab$.subscribe(
      (users) => {
        this.userTab = users;
        this.connectedUser = this.initAdminUserArray(users,this.arrAdminUserObj, this.userId);
      });
    this.authService.getAllUser(this.USERS_URL);
  }
  initBackGroundColor(){
    this.changeBackgroundColorSub = this.navLoginRegisterService.buttonFormColor$.subscribe(
      (hasChanged)=> {
        this.changeBackgroundColor= hasChanged;
      })
  }
  initSignupForm(){
     this.signupForm = this.formBuilder.group({
     firstName        : [null, Validators.required],
     name             : [null, Validators.required],
     email            : [null, [Validators.required, Validators.email]],
     password         : [null, Validators.required],
     confirmPassword  : [null, Validators.required]
    });
  }
  initSigninForm(){
      this.signInForm = this.formBuilder.group({
      email          : [null, [Validators.required, Validators.email]],
      password       : [null, Validators.required]
    });
  }
  initLogoFom(){
    this.logoForm = this.formBuilder.group({
      title         : [null, Validators.required],
      description   : [null, Validators.required],
      image         : [null, Validators.required, mimeType]
    })
  }
  initLogoVideoList(){
    this.loading         = true;
    this.logoVideoSub    = this.logoVideoService.logoVideo$.subscribe(
      (logoVideoTab) => {
        this.logoVideoTab= logoVideoTab
        // store ASLOGO/DEFAULT
        this.initLogoArray(logoVideoTab,this.arrObj);

        // display ASLOGO if ASLOGO/DEFAULT are both presents
        if( this.arrObj.hasOwnProperty(environment.ASLOGO) &&
        this.arrObj.hasOwnProperty(environment.DEFAULT)){
          this.logo = this.arrObj[environment.ASLOGO];
        }
        // display DEFAULT LOGO
        else {
          this.logo = this.arrObj[environment.DEFAULT];
        }
        this.loading=false;
      }
    );
    this.logoVideoService.getLogoVideo(this.URL);
  }
  updateLogo(){
    const logoObj    = new LogoBackGrdVideo();
    for(let logo of this.logoVideoTab){
      if (logo.title == 'ASLOGO'){
        logoObj._id  = logo._id;
        logoObj.title= 'old_'+ logo.title;
        // mv ASLOGO > old_ASLOGO
        this.crudService.modifyProductWithFile(logo._id, logoObj,
          logo.imageUrl, this.URL, environment.APPEND_PRODUCT).then(
          () => {
            this.newLogo();
            this.router.navigate([environment.PRODUCTS_URL]);
            this.authService.JoinAndClose();
          },
          (error) => {
            this.loading = false;
            this.errorMessage = error.message;
          }
        )
      }
    }
  }
  newLogo(){
    // mv checked logo > ASLOGO
    const newLogoObj    = new LogoBackGrdVideo();
    newLogoObj._id      = this.checkedLogoId;
    newLogoObj.title    = environment.ASLOGO;
    newLogoObj.imageUrl = this.checkedImageUrl;
    this.crudService.modifyProductWithFile(this.checkedLogoId, newLogoObj,
      newLogoObj.imageUrl, this.URL, environment.APPEND_PRODUCT).then(
        () => {
          this.router.navigate([environment.PRODUCTS_URL]);
        },
        (error) => {
          this.loading      = false;
          this.errorMessage = error.message;
        }
      )
  }
  checkFileExt(fileName:string){
    return this.crudService.checkFileExt(fileName);
  }
  onSubmit(): void{
    this.loading = true;
    const logo = new LogoBackGrdVideo();
    logo.title = this.logoForm.get('title').value;
    logo.description = this.logoForm.get('description').value;
    logo.imageUrl = ' ';
    logo.userId = this.authService.getUserId();
    var element = <HTMLInputElement> document.getElementById("update");
    // Start Check ASLOGO exist before adding
    const arr :any = this.initLogoArray(this.logoVideoTab,this.arrObj);
    if (element.checked || (logo.title ==environment.ASLOGO)){
      logo.title =environment.ASLOGO;
      this.checkUpdate(arr[environment.ASLOGO+'_ID'], this.URL);
    }
    else if (element.checked || (logo.title == 'DEFAULT')){
      logo.title ='DEFAULT';
      this.checkUpdate(arr[environment.DEFAULT+'_ID'], this.URL);
    }
    // End Check ASLOGO exist before adding

    this.crudService.createNewProductWithFile(logo,this.logoForm.get('image').value,
    this.URL,environment.APPEND_PRODUCT).then(
      () => {
        this.logoForm.reset();
        this.loading = false;
        this.authService.JoinAndClose();
      },
      (error) => {
        this.loading = false;
        this.errorMessage = error.message;
      })
  }
  checkUpdate(logoId:string,serverUrl: string){
    this.crudService.deleteProduct(logoId, serverUrl).then(
      () => {
        this.router.navigate([environment.PRODUCTS_URL]);
      })
  }

  initLogoArray(tab:any,arrayObj:any): any {
    for(let obj of tab){
      if(obj.title==environment.ASLOGO ||
      obj.title==environment.DEFAULT ){
        arrayObj[obj.title]=obj.imageUrl;
        arrayObj[obj.title+'_ID']=obj._id;
      }
    }
    return arrayObj;
  }
  initAdminUserArray(tab:any,arrayObj:any, id: string): any {
    for(let obj of tab['user']){
      if(obj.role==environment.ADMIN_USER){
        arrayObj[obj._id]=obj.email;
      }
    }
    return arrayObj.hasOwnProperty(id);
  }

  onImagePick(event: Event){
    this.crudService.onImagePick(event,this.logoForm);
  }

  onSelectCheckboxLogo(id:string,imageUrl:string) {
    let cbs =  document.getElementsByClassName("checkbox");
    let checkElement = <HTMLInputElement> document.getElementById(id);
      for (let i = 0; i < cbs.length; i++) {
        let checkedElementId = <HTMLInputElement> document.getElementById(cbs[i].id);
        checkedElementId.checked = false;
      }
      checkElement.checked = true;
      this.isLogoChecked   = checkElement.checked;
      this.checkedLogoId   = id;
      this.checkedImageUrl = imageUrl
  }

  header_variable=true;
  toggleSideNav() {
    this.navService.setShowNav(true);
  }
  onSidebarClose() {
    this.navService.setShowNav(false);
  }
  toggleLoginSideNav() {
    this.navLoginRegisterService.setShowLoginForm(true);
  }
  onLoginSidebarClose() {
    this.navLoginRegisterService.setShowLoginForm(false);
  }
  openContent(content) {
    this.authService.open(content);
  }

  emitButton(){
    this.isButtonVisibleSub = this.navService.showNav$.subscribe(
    (data)=>{
      this.isButtonVisible=data;
    })
  }
  initAuthSub(){
  this.tokenSub = this.authService.token$.subscribe(
  (auth)=>{
    this.token= auth;
    })
  }
  @HostListener('window:resize', ['$event'])
  onResize() {
  this.screenWidthSub = this.sizeDetectorService.screenWidth$.subscribe(
    ()=> {
      this.screenWidth = window.innerWidth;
      switch(true){
        case(this.screenWidth < 576):
          this.screenWidthCode = 'vsm';
          break;
        case(this.screenWidth >= 576 && this.screenWidth <= 768):
          this.screenWidthCode = 'sm';
          break;
          case(this.screenWidth >= 768 && this.screenWidth <= 992):
            this.screenWidthCode = 'md';
            break;
          case(this.screenWidth >= 992 && this.screenWidth <= 1200):
          this.screenWidthCode  = 'lg';
          break;
        case(this.screenWidth  >= 1200):
          this.screenWidthCode = 'xl';
          break;
        default:
          break;
      }
    })
  }
  onSignup() {
    this.loading = true;
    const name = this.signupForm.get('name').value;
    const firstName = this.signupForm.get('firstName').value;
    const email = this.signupForm.get('email').value;
    const password = this.signupForm.get('password').value;
    const role : Role = Role.role;
    const confirmPassword = this.signupForm.get('confirmPassword').value;

    if(password !== confirmPassword){
    throw new Error('password are different')
    }
    else{
      this.authService.createUser(name, firstName, email, password, role).then(
      () => {
      this.loading = false;
        this.router.navigate([environment.PRODUCTS_URL]);
      })
      .catch(
        (error) => {
          this.loading = false;
          this.errorMessage = error.message;
        });
      if(password === confirmPassword){
          this.authService.JoinAndClose();
      }
    }
  }
  onSignIn(){
  this.loading = true;
  const email = this.signInForm.get('email').value;
  const password = this.signInForm.get('password').value;
  this.authService.login(email, password).then(
    () => {
      this.loading = false;
        this.router.navigate([environment.PRODUCTS_URL]);
        this.authService.JoinAndClose();
    }).catch(
    (error) => {
      this.loading = false;
      this.errorMessage = error.message;
    });
  }
  openTrue(){
  this.navLoginRegisterService.setShowNavColor(true);
  }
  openFalse(){
    this.navLoginRegisterService.setShowNavColor(false);
  }
  changeBackGroundColor(tag:string){
    if(tag === 'login' || tag === 'upload'){
      if(this.changeBackgroundColor){
      return "blue";
      }
    }
    if(tag === 'register' || tag === 'update'){
      if(!this.changeBackgroundColor){
        return "blue";
      }
    }
  }
  onNavigate(url: string){
  this.router.navigate([url]);
  }
  isNotNullAndNotEmpty(var0: string){
    return this.util.isNotNullAndNotEmpty(var0);
  }
  isNullOrEmpty(var0: string){
    return this.util.isNullOrEmpty(var0);
  }
  onLogout(){
  this.authService.logout();
  this.router.navigate([environment.PRODUCTS_URL]);
  }
  ngOnDestroy() {
    this.tokenSub.unsubscribe();
    this.isButtonVisibleSub.unsubscribe();
    this.screenWidthSub.unsubscribe();
    this.logoVideoSub.unsubscribe();
    this.changeBackgroundColorSub.unsubscribe();
    this.userTabSub.unsubscribe();
  }
}
