import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { SideBarNavigationService } from 'src/app/services/side-bar-navigation.service';
import { Observable, Subscription } from 'rxjs';
import { SideNavDirection } from '../side-nav-direction';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SideNavComponent implements OnInit {

  showSideNav: Observable<boolean>;
  isButtonVisible: boolean;
  buttonSub: Subscription;

  @Input() sideNavTemplateRef: any;
  @Input() duration: number=0.25;
  @Input() navWidth: number = window.innerWidth;
  @Input() direction: SideNavDirection = SideNavDirection.Left;

  constructor(private navService: SideBarNavigationService) { }
  ngOnInit(): void {
    this.showSideNav = this.navService.getShowNav();
    this.initButton();
  }
  initButton(){
   this.navService.showNav$.subscribe(
   (show)=>{
    this.isButtonVisible=show;
   })
  }
  onSidebarClose() {
    this.navService.setShowNav(false);
  }

  getSideNavBarStyle(showNav: boolean) {
    let navBarStyle: any = {};

    navBarStyle.transition = this.direction + ' ' + this.duration + 's, visibility ' + this.duration + 's';
    navBarStyle.width = this.navWidth + 'px';
    navBarStyle[this.direction] = (showNav ? 0 : (this.navWidth * -1)) + 'px';

    return navBarStyle;
  }
}
