import { HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Subscription } from 'rxjs';

@Injectable()
export class AuthInterceptor implements HttpInterceptor, OnInit, OnDestroy{
tokenSub: Subscription;
token: string;
  constructor(private auth: AuthService) {this.initTokenSub()}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    const newRequest = req.clone({
      headers: req.headers.set('Authorization', 'Bearer ' + this.token)
    });
    return next.handle(newRequest);
  }
  initTokenSub(){
    this.tokenSub = this.auth.token$.subscribe(
    (auth)=>{
      this.token= auth;
    })
   }

  ngOnInit() {
    this.initTokenSub();
 }
 ngOnDestroy(){
  this.tokenSub.unsubscribe();
 }
}
