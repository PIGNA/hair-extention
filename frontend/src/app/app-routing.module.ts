import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductsComponent } from './products/products.component';
import { ExtensionListComponent } from './products/extension-list/extension-list.component';
import { SingleProductComponent } from './products/single-product/single-product.component';
import { NewExtensionWithUploadComponent } from './products/new-extension-with-upload/new-extension-with-upload.component';
import { AuthGuardService } from './services/auth-guard.service';
import { environment } from '../environments/environment';
import { ModifyProductComponent } from './products/modify-product/modify-product.component';

const routes: Routes = [
  { path: environment.PRODUCTS, component: ProductsComponent,
    children: [
      { path: environment.NEW_PRODUCT, component: NewExtensionWithUploadComponent, canActivate: [AuthGuardService] },
      { path: environment.ALL_PRODUCTS, component: ExtensionListComponent},
      { path: environment.PRODUCT + environment.separator + ':id', component: SingleProductComponent},
      { path: environment.MODIFY_PRODUCTS + ':id', component: ModifyProductComponent, canActivate: [AuthGuardService]},
      { path: environment.PRODUCTS, pathMatch: 'full', redirectTo: environment.ALL_PRODUCTS },
      { path: '**', redirectTo: environment.ALL_PRODUCTS }

    ]
    },
  { path: environment.ALL_PRODUCTS, component: ExtensionListComponent },
  { path: environment.PRODUCTS, pathMatch: 'full', component: ExtensionListComponent },
  { path: '**', redirectTo: environment.PRODUCTS }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)
  ],
   exports: [
      RouterModule
    ],
   providers: [
      AuthGuardService
    ]
})
export class AppRoutingModule { }
