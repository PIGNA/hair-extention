import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CrudService } from '../../services/crud.service';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { Extension } from '../../models/Extension.models';
import { mimeType } from '../mime-type.validator';
import { environment } from '../../../environments/environment';
@Component({
  selector: 'app-new-extension-with-upload',
  templateUrl: './new-extension-with-upload.component.html',
  styleUrls: ['./new-extension-with-upload.component.scss']
})
export class NewExtensionWithUploadComponent implements OnInit {
 public  extensionForm: FormGroup;
  public loading = false;
  public userId: string = this.auth.getUserId();
  public errorMessage: string;
  private URL = environment.ROOT_URL + environment.SERVER_PORT + environment.SERVER_URL_PRODUCT;
  constructor(
              private formBuilder: FormBuilder,
              private crudService: CrudService,
              private router: Router,
              private auth: AuthService) {
                console.log('New extension comp => userID: ' + this.userId);
               }


  ngOnInit(): void {
    this.extensionForm = this.formBuilder.group({
      title: [null, Validators.required],
      description: [null, Validators.required],
      price: [0, Validators.required],
      image: [null, Validators.required, mimeType]
   });
 }
  onSubmit(){
    this.loading = true;
    const extension = new Extension();
    extension.title = this.extensionForm.get('title').value;
    extension.description = this.extensionForm.get('description').value;
    extension.price = this.extensionForm.get('price').value * 100;
    extension.imageUrl = '';
    extension.userId = this.userId;

   this.crudService.createNewProductWithFile(extension, this.extensionForm.get('image').value,this.URL,environment.APPEND_PRODUCT).then(
      () => {
        this.extensionForm.reset();
        this.loading = false;
        this.router.navigate([environment.PRODUCTS_URL]);
      },
      (error) => {
        this.loading = false;
        this.errorMessage = error.message;
      }
    );
  }

    onImagePick(event: Event) {
      this.crudService.onImagePick(event,this.extensionForm);
    }
}
