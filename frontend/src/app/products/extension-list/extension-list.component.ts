import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ExtensionService } from '../../services/extension.service';
import { Subscription } from 'rxjs';
import { Extension } from '../../models/Extension.models';
import { environment } from '../../../environments/environment';
import { CrudService } from 'src/app/services/crud.service';
@Component({
  selector: 'app-extension-list',
  templateUrl: './extension-list.component.html',
  styleUrls: ['./extension-list.component.scss']
})
export class ExtensionListComponent implements OnInit, OnDestroy {
  separator = "/";
  public productTab : Extension[] = [];
  private productSub: Subscription;
  public loading      : boolean;
  private URL = environment.ROOT_URL + environment.SERVER_PORT + environment.SERVER_URL_PRODUCT;

  constructor(private productService: ExtensionService,
              private crudService   : CrudService,
              private router        : Router) {
               }

  ngOnInit() {
    this.loading         = true;
    this.productSub    = this.productService.product$.subscribe(
      (product) => {
        this.productTab= product;
        this.loading     = false;
      }
    );
     this.productService.getProduct(this.URL);
  }
  getPrice(price:number){
    return new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(price);
  }

  checkFileExt(fileName:string){
    return this.crudService.checkFileExt(fileName);
  }

    onProductClicked(id: string) {
      this.router.navigate([environment.PRODUCTS + this.separator + environment.PRODUCT + this.separator + id]);
  }

  scrollToElement($element): void {
    $element.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
  }
  ngOnDestroy(): void {
    this.productSub.unsubscribe();
  }
}
