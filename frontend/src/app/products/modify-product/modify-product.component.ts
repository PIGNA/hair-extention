import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { mimeType } from '../mime-type.validator';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Extension } from '../../models/Extension.models';
import { environment } from '../../../environments/environment';
import { CrudService } from '../../services/crud.service';

@Component({
  selector: 'app-modify-product',
  templateUrl: './modify-product.component.html',
  styleUrls: ['./modify-product.component.scss']
})
export class ModifyProductComponent implements OnInit {
  public productForm: FormGroup;
  public product    : Extension;
  public loading      = false;
  public part         : number;
  public userId       : string;
  public imagePreview : string;
  public errorMessage : string;
  private URL = environment.ROOT_URL + environment.SERVER_PORT + environment.SERVER_URL_PRODUCT;
  constructor(
    private authService   : AuthService,
    private formBuilder   : FormBuilder,
    private route         : ActivatedRoute,
    private router        : Router,
    private crudService   : CrudService) { }

  ngOnInit(): void {
    this.productToBeModifyed();
  }
  productToBeModifyed(): void {
    this.loading = true;
    this.userId  = this.authService.getUserId();
    this.route.params.subscribe(
      (params) => {
        this.crudService.getProductById(params.id,this.URL).then(
          (product: Extension) => {
            this.product     = product;
            this.productForm = this.formBuilder.group({
              title          : [product.title, Validators.required],
              description    : [product.description, Validators.required],
              price          : [product.price / 100, Validators.required],
              image          : [product.imageUrl, Validators.required, mimeType]
            });
            this.imagePreview= product.imageUrl;
            this.loading     = false;
          }
        );
      }
    );
  }

  onSubmit(): void {
    this.loading       = true;
    const product      = new Extension();
    product._id        = this.product._id;
    product.title      = this.productForm.get('title').value;
    product.description= this.productForm.get('description').value;
    product.price      = this.productForm.get('price').value * 100;
    product.imageUrl   = '';
    product.userId     = this.userId;
    this.crudService.modifyProductWithFile(this.product._id, product, this.productForm.get('image').value, this.URL, environment.APPEND_PRODUCT).then(
      () => {
        this.productForm.reset();
        this.loading = false;
        this.router.navigate([environment.PRODUCTS_URL]);
      },
      (error) => {
        this.loading = false;
        this.errorMessage = error.message;
      }
    );
  }
  onImagePick(event: Event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.productForm.get('image').patchValue(file);
    this.productForm.get('image').updateValueAndValidity();
    const reader = new FileReader();
    reader.onload = () => {
      if (this.productForm.get('image').valid) {
        this.imagePreview = reader.result as string;
      } else {
        this.imagePreview = null;
      }
    };
    reader.readAsDataURL(file);
  }
}
