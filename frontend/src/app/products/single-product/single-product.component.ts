import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router, } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { UtilService } from '../../services/util.service';
import { CrudService } from '../../services/crud.service';
import { Extension } from '../../models/Extension.models';
import { environment } from '../../../environments/environment';


@Component({
  selector: 'app-single-product',
  templateUrl: './single-product.component.html',
  styleUrls: ['./single-product.component.scss']
})
export class SingleProductComponent implements OnInit {
  public loading: boolean;
  public extension: Extension;
  public userId: string;
  private URL = environment.ROOT_URL + environment.SERVER_PORT + environment.SERVER_URL_PRODUCT;

  constructor(private router: Router,
    private route      : ActivatedRoute,
    private crudService:CrudService,
    private auth       : AuthService,
    private util       : UtilService) {
}

  ngOnInit(): void {
    this.loading = true;
    this.getRequestProductId();
  }
  getRequestProductId(): void{
    this.userId = this.auth.getUserId();
    this.route.params.subscribe(
      (params:Params) => {
        this.crudService.getProductById(params.id, this.URL).then(
          (extension:Extension) => {
            this.loading = false;
            this.extension = extension;
          }
        )
      }
    )
  }
  updateProduct(): void{
    this.loading = true;
    this.router.navigate([environment.separator + environment.PRODUCTS + environment.separator + environment.MODIFY_PRODUCTS + this.extension._id])
  }

  deleteProduct() : void{
    this.loading = true;
    this.crudService.deleteProduct(this.extension._id, this.URL).then(
      () => {
        this.router.navigate([environment.PRODUCTS_URL]);
      }
    )
  }
  isNotNullAndNotEmpty(var0: string){
    return this.util.isNotNullAndNotEmpty(var0);
  }
}
