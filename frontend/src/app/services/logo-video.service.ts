import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { LogoBackGrdVideo } from '../models/LogoBackGrdVideo.models';
import { CrudService } from '../services/crud.service';

@Injectable({
  providedIn: 'root'
})
export class LogoVideoService {
  private logoVideo  :LogoBackGrdVideo[];
  public logoVideo$  =new Subject<LogoBackGrdVideo[]>();
  constructor(private crudService: CrudService) { }

  getLogoVideo(url:string) {
    this.crudService.getProduct(url,this.logoVideo, this.logoVideo$);
  }
}
