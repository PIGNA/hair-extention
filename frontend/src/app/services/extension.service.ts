import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Extension } from '../models/Extension.models';
import { CrudService } from '../services/crud.service';

@Injectable({
  providedIn: 'root'
})
export class ExtensionService {
  private product: Extension[];
  public product$ = new Subject<Extension[]>();
  constructor(private crudService: CrudService) { }

  getProduct(url:string) {
    this.crudService.getProduct(url,this.product, this.product$);
  }
}

