import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot,CanActivate, Router } from '@angular/router';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(private router: Router,
              private auth: AuthService) {}
  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    return new Observable(
      (observer) => {
        this.auth.userRole$.subscribe(
          (role) => {
            role = this.auth.userRole;
              if ( role !== 'Admin') {
                this.router.navigate([environment.PRODUCTS_URL]);
             }
            observer.next(true);
          }
        );
      }
    );
  }
}
