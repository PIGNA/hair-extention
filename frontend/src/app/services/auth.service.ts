import { Injectable, OnDestroy, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Subscription } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { Role } from '../auth/sign/role';
import { environment } from '../../environments/environment';
import { User } from '../models/User.models';
import{ Subject} from 'rxjs';
import { CrudService } from '../services/crud.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements OnInit, OnDestroy {
  private rootURL  = environment.ROOT_URL + environment.SERVER_PORT;
  private urlSignin= this.rootURL + environment.SERVER_URL_SIGNIN;
  private urlSignup= this.rootURL + environment.SERVER_URL_SIGNUP;
  modalReference   : NgbModalRef;
  userRole$        = new BehaviorSubject<string>(this.getRole());
  userRoleSub      : Subscription;
  userRole         : string = this.getRole();
  token$           = new BehaviorSubject<string>(this.getToken());
  token            : string = this.getToken();
  tokenSub         : Subscription;
  userId           : string = this.getUserId();
  userId$          = new BehaviorSubject<string>(this.getUserId());
  userIdSub        : Subscription;
  closeResult      : string;
  usersTab         : User[];
  usersTab$        = new Subject<User[]>();

  constructor(private modalService : NgbModal,
              private cookieService: CookieService,
              private crudService  : CrudService,
              private http         : HttpClient,
              private router        : Router) {}
ngOnInit() {
    this.initSubsCription(this.userRole,this.userRole$,this.userRoleSub);
    this.initSubsCription(this.token,this.token$,this.tokenSub);
    this.initSubsCription(this.userId,this.userId$,this.userIdSub);
}

open(content) {
  this.modalReference = this.modalService.open(content);
  this.modalReference.result.then((result) => {
  this.closeResult    = `Closed with: ${result}`;
  }, (reason)         => {
    this.closeResult  = `Dismissed ${this.getDismissReason(reason)}`;
  });
}
 getRole() : string{
  return this.cookieExist(environment.ROLE);
 }
 getToken(): string {
  return this.cookieExist(environment.TOKEN);
 }
 getUserId(): string {
  return this.cookieExist(environment.ID);
 }

cookieExist(cookie: string) : string {
  const cookieExists: boolean = this.cookieService.check(cookie);
  if(!cookieExists){
    this.cookieService.set(cookie,' ',environment.COOKIE_TIME,environment.COOKIE_PATH);
  return this.cookieService.get(cookie);
}
  else{
    return this.cookieService.get(cookie);
  }
 }

 private getDismissReason(reason: any): string {
     if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    }
    else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
     return 'by clicking on a backdrop';
    }
    else {
     return `with: ${reason}`;
    }
 }
  JoinAndClose() {
    this.modalReference.close();
  }
  createUser(name: string, firstName: string, email: string, password: string, role: Role){
      return new Promise<void>((resolve, reject) => {
      this.http.post(
        this.urlSignup,
        {name: name, firstName: firstName, email: email, password: password, role: role})
        .subscribe(
          () => {
            this.login(email, password).then(
              () => {
                resolve();
              }
            ).catch(
            (error) => {
              reject(error);
            }
            );
          },
          (error) => {
            reject(error);
          }
        );
      });
  }
  login(email: string, password: string) {
    return new Promise<void>((resolve, reject) => {
      this.http.post(
      this.urlSignin,
      {email: email, password: password})
      .subscribe(
        (authData: {token: string, userId: string, role: string}) => {
          this.token = authData.token;
          this.userId = authData.userId;
          this.userRole = authData.role;

          this.token$.next(authData.token);
          this.userId$.next(authData.userId);

          // start create new cookies
          this.cookieService.set(environment.ROLE, this.userRole,environment.COOKIE_TIME,environment.COOKIE_PATH);
          this.cookieService.set(environment.TOKEN, this.token,environment.COOKIE_TIME,environment.COOKIE_PATH);
          this.cookieService.set(environment.ID, this.userId,environment.COOKIE_TIME,environment.COOKIE_PATH);
          // end create new cookie
          resolve();
        },
        (error) => {
          reject(error);
        }
      );
    });
  }
  getAllUser(url:string){
    this.crudService.getProduct(url,this.usersTab,this.usersTab$);
  }

  initSubsCription(observer: string, observable$: BehaviorSubject<string>, sub: Subscription){
    sub = observable$.subscribe(
      (variable) => {
      observer = variable;
      observable$.next(observer);
      console.log('subscription: ' + sub);
      }
    )
  }


  logout(){
     this.cookieService.delete(environment.ROLE,environment.COOKIE_PATH,environment.DOMAIN);
     this.cookieService.delete(environment.TOKEN,environment.COOKIE_PATH,environment.DOMAIN);
     this.cookieService.delete(environment.ID,environment.COOKIE_PATH,environment.DOMAIN);
     this.userRole$.next(' ');
     this.token$.next(' ');
     this.userId$.next(' ');
    }
   ngOnDestroy() {
     this.userRoleSub.unsubscribe();
     this.tokenSub.unsubscribe();
     this.userIdSub.unsubscribe();
    }
}
