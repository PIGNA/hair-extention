import { Injectable, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { BehaviorSubject } from 'rxjs'
@Injectable({
  providedIn: 'root'
})
export class RegisterModalFormService implements OnInit{

  showLoginForm$   : BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  showRegisterForm$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  buttonFormColor$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
  constructor(private router: Router) {
    router.events.subscribe(() => {
      this.setShowLoginForm(false);
      this.setShowRegisterForm(false);
      this.setShowNavColor(true);
    })
  }

  getShowLoginForm(){
    return this.showLoginForm$.asObservable();
  }
  setShowLoginForm(showHide: boolean){
    this.showLoginForm$.next(showHide);
  }

  getShowRegisterForm(){
    return this.showRegisterForm$.asObservable();
  }
  setShowRegisterForm(showHide: boolean){
    this.showRegisterForm$.next(showHide);
  }
  setShowNavColor(activeColor: boolean){
    this.buttonFormColor$.next(activeColor);
  }
  getShowNavColor(){
    return this.buttonFormColor$.asObservable();
  }
  ngOnInit(){}
}
