import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  constructor() { }

  public isNullOrEmpty(var0: string) :boolean {
    return var0 === null || var0.trim().length === 0 || var0.toLocaleLowerCase() === ' ';
  }
  public isNotNullAndNotEmpty(var0: string) : boolean {
    return !this.isNullOrEmpty(var0);
  }
}
