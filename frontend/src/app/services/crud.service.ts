import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { UtilService } from '../services/util.service';
@Injectable({
  providedIn: 'root'
})
export class CrudService {

  constructor(private http: HttpClient, private util: UtilService) { }

  createNewProductWithFile(DMproduct: any, image: File, url:string,appendObj: string) {
    return new Promise((resolve, reject) => {
      const productData = new FormData();
      productData.append(appendObj, JSON.stringify(DMproduct));
      productData.append('image', image, DMproduct.title);
      this.http.post(url, productData).subscribe(
        (response) => {
          resolve(response);
        },
        (error) => {
         reject(error);
        }
      );
    });
  }

  getProduct(url:string,Tab: any[],Tab$:Subject<any[]>) {
    this.http.get(url).subscribe(
      (productTab: any[]) => {
        if (productTab) {
          Tab = productTab;
          Tab$.next(Tab);
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  getProductById(id: string, url: string) {
    return new Promise((resolve, reject) => {
      this.http.get(url +'/'+ id).subscribe(
        (response) => {
          resolve(response);
        },
        (error) => {
          reject(error);
        }
      );
    });
  }
  modifyProductWithFile(id: string, DMObj: any, image: File | string, url: string, appendObj: string) {
    return new Promise((resolve, reject) => {
      let productData: any | FormData;
      if (typeof image === 'string') {
        DMObj.imageUrl = image;
        productData = DMObj;
      } else {
        productData = new FormData();
        productData.append(appendObj, JSON.stringify(DMObj));
        productData.append('image', image, DMObj.title);
      }
      this.http.put(url +'/'+ id, productData).subscribe(
        (response) => {
          resolve(response);
        },
        (error) => {
          reject(error);
        }
      );
    });
  }
  deleteProduct(id: string, url: string) {
    return new Promise((resolve, reject) => {
      this.http.delete(url +'/'+ id).subscribe(
        (response) => {
          resolve(response);
        },
        (error) => {
          reject(error);
        }
      );
    });
  }
  onImagePick(event: Event, objForm: FormGroup) {
    event.preventDefault();
    const file = (event.target as HTMLInputElement).files[0];
    objForm.get('image').patchValue(file);
    objForm.get('image').updateValueAndValidity();
    const reader = new FileReader();
    reader.readAsDataURL(file);
  }

  checkFileExt(fileName:string){
        let notAllowedExtension = /[^.]+$/.exec(fileName);
        switch (notAllowedExtension.toString()) {
          case 'mp4':
          case 'undefined':
          case 'mp3':
          break;
          default:
            return fileName;
        }

      }

}
