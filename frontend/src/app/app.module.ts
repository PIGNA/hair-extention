import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { SideNavComponent } from './header/side-nav/side-nav.component';
import { HomeComponent } from './home/home.component';
import { SignComponent } from './auth/sign/sign.component';
import { AuthInterceptor } from './interceptors/auth-interceptor';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ProductsComponent } from './products/products.component';
import { NewExtensionWithUploadComponent } from './products/new-extension-with-upload/new-extension-with-upload.component';
import { ExtensionListComponent } from './products/extension-list/extension-list.component';
import { SingleProductComponent } from './products/single-product/single-product.component';
import { ModifyProductComponent } from './products/modify-product/modify-product.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SideNavComponent,
    HomeComponent,
    SignComponent,
    ProductsComponent,
    NewExtensionWithUploadComponent,
    ExtensionListComponent,
    SingleProductComponent,
    ModifyProductComponent
      ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
