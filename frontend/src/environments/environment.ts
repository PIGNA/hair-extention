// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
export const environment = {
  ADMIN_USER: 'Admin',
  STD_USER:'USER',
  separator: "/",
  production: false,
  ROOT_URL:"http://localhost:",
  SERVER_PORT: 3921,
  SERVER_URL_PRODUCT: "/beautiful/product/extension",
  SERVER_URL_USER: "/beautiful/user",
  SERVER_URL_LOGO_BCKVIDEO:  "/beautiful/product/logo",
  SERVER_URL_SIGNIN: "/beautiful/user/signin",
  SERVER_URL_SIGNUP: "/beautiful/user/signup",
  PRODUCTS_URL: "/products/all-extension/",
  ADD_PRODUCT: "products/new-extension",

  //App Routing Module
  PRODUCTS: "products",
  NEW_PRODUCT: "new-extension",
  ALL_PRODUCTS: "all-extension",
  PRODUCT: "tuesbelle",
  MODIFY_PRODUCTS: "modify/product/",
  HOME: "home",

  // cookie
  COOKIE_TIME: 1,
  COOKIE_PATH: '/',
  DOMAIN: 'localhost',
  ROLE: 'userRole',
  TOKEN: 'token',
  ID: 'userId',

  // request collections url
  APPEND_PRODUCT: "product",
  // tags
  ASLOGO:"ASLOGO",
  DEFAULT:"DEFAULT"

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
