# Frontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

1 - Generate ng project
ng new frontend --style=scss --skip-tests=true
2 - ng g c xxxxx
3 - install bootstrap and in angular.json/architect
##"styles":
 ##[
 ##   "./node_modules/bootstrap/dist/css/bootstrap.css",
 ##  "src/styles.scss"
##]
4 - install  rxjs-compat
5 - ng add @ng-bootstrap/ng-bootstrap
6 -  mongo "mongodb+srv://cluster0.dr8ms.mongodb.net/Cluster0" --username <db_name>
7 -  Upgrade dependencies
     npx npm-check-updates -u --packageFile package.json
     npm install
